import React, { useContext, useState } from "react";
import noteContext from "../context/notes/noteContext";

const AddNotes = (props) => {
  const context = useContext(noteContext);
  const { addNote } = context;
  const [input, setInput] = useState({
    title: "",
    description: "",
    tag: "",
  });
  const onChangeInput = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };
  const handleClick = (e) => {
    e.preventDefault();
    addNote(input.title, input.description, input.tag);
    setInput({
      title: "",
      description: "",
      tag: "",
    });
    props.showAlert("Added Successfully", "success");
  };
  return (
    <div>
      <div className="container my-3">
        <h2>Add a Note</h2>
        <form className="my-2">
          <div className="mb-3">
            <label htmlFor="title" className="form-label">
              Title
            </label>
            <input
              type="text"
              className="form-control"
              id="title"
              name="title"
              aria-describedby="emailHelp"
              onChange={onChangeInput}
              value={input.title}
              minLength={5}
              required
            />
          </div>
          <div className="mb-3">
            <label htmlFor="description" className="form-label">
              Description
            </label>
            <input
              type="text"
              className="form-control"
              id="description"
              name="description"
              onChange={onChangeInput}
              value={input.description}
              minLength={5}
              required
            />
          </div>
          <div className="mb-3">
            <label htmlFor="tag" className="form-label">
              Tag
            </label>
            <input
              type="text"
              className="form-control"
              id="tag"
              name="tag"
              onChange={onChangeInput}
              value={input.tag}
            />
          </div>
          <button
            disabled={input.title.length < 5 || input.description.length < 5}
            type="submit"
            className="btn btn-primary"
            onClick={handleClick}
          >
            Add Notes
          </button>
        </form>
      </div>
    </div>
  );
};

export default AddNotes;
