const express = require("express");
const router = express.Router();
const fetchUser = require("../middleware/fetchUser");
const Notes = require("../models/Notes");
const { body, validationResult } = require("express-validator");

//ROUTE 1: Get all notes using: GET "/api/notes/fetchAllNotes" Login required
router.get("/fetchAllNotes", fetchUser, async (req, res) => {
  try {
    //Fetching all notes based on user id.
    const notes = await Notes.find({ user: req.user.id });
    res.json(notes);
  } catch (error) {
    //Catch error
    console.error(error.message);
    res.status(500).send("Some error occured");
  }
});

//ROUTE 2: Add a new note using: POST "/api/notes/addNotes" Login required
router.post(
  "/addNotes",
  fetchUser,
  [
    body("title", "Enter a valid title").isLength({ min: 3 }),
    body("description", "Description must be atleast 5 characters").isLength({
      min: 5,
    }),
  ],
  async (req, res) => {
    try {
      const { title, description, tag } = req.body;

      //If there are erroes return bad request.
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      const note = new Notes({
        title,
        description,
        tag,
        user: req.user.id,
      });

      const savedNotes = await note.save();
      res.json(savedNotes);
    } catch (error) {
      //Catch error
      console.error(error.message);
      res.status(500).send("Some error occured");
    }
  }
);

//ROUTE 3: Updating an exisitng note using: PUT "/api/notes/updateNotes" Login required
router.put("/updateNotes/:id", fetchUser, async (req, res) => {
  try {
    const { title, description, tag } = req.body;

    //create a newNote object
    const newNote = {};
    if (title) {
      newNote.title = title;
    }
    if (description) {
      newNote.description = description;
    }
    if (tag) {
      newNote.tag = tag;
    }

    //Find the note to be updated and update it
    let note = await Notes.findById(req.params.id);

    //If note was not found
    if (!note) {
      return res.status(404).send("Not Found");
    }

    //Checking if notes belong to the user or not
    if (note.user.toString() !== req.user.id) {
      return res.status(401).send("Not Allowed");
    }

    //If note found and belongs to user
    note = await Notes.findByIdAndUpdate(
      req.params.id,
      { $set: newNote },
      { new: true }
    );
    res.json({ note });
  } catch (error) {
    //Catch error
    console.error(error.message);
    res.status(500).send("Some error occured");
  }
});

//ROUTE 4: Deleting an exisitng note using: DELETE "/api/notes/deleteNotes" Login required
router.delete("/deleteNotes/:id", fetchUser, async (req, res) => {
  try {
    //Find the note to be deleted and delete it
    let note = await Notes.findById(req.params.id);

    //If note was not found
    if (!note) {
      return res.status(404).send("Not Found");
    }

    //Checking if notes belong to the user or not
    if (note.user.toString() !== req.user.id) {
      return res.status(401).send("Not Allowed");
    }

    //If note found and belongs to user
    note = await Notes.findByIdAndDelete(req.params.id);
    res.json({ Success: "Successfully deleted the note", note: note });
  } catch (error) {
    //Catch error
    console.error(error.message);
    res.status(500).send("Some error occured");
  }
});

module.exports = router;
